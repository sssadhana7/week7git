
public class AddOdd 
{

	public static void main(String[] args)
	{
	System.out.println("Sum of 50 odd numbers");
	int c=0;
	int b=0;
	for(int a=1; a<=50; a++)//tip:do nt use tis FOR LOOP variable 'a' inside curly braces{} on LHS while performing operations, 
	 {						//can use t on RHS.Bcoz thr 'll nt b an end. it 'll nt throw an error bt o/p s nt possible
		c=a%2;				//instead introduce another dummy variable and use it on LHS
		if(c==1)			//dont: 
		{					//a=a%2;
			b=b+a;			//if(a==1)
		}					//{b=b+a};
	}
	System.out.println(b);
}

}
